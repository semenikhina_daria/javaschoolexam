package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;


public class Subsequence {

    public boolean find(List x, List y) throws Exception {


        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        int kx = x.size();
        int kp = 0;
        int index = 0;

        if (x.isEmpty()) {
            return true;
        } else {

            for (int i = 0; i < x.size(); i++) {

                for (int j = index; j < y.size(); j++) {
                    if (x.get(i) == y.get(j)) {
                        kp++;
                        index = j + 1;
                        break;
                    }

                }
            }


            return kp == kx;
        }
    }

}


