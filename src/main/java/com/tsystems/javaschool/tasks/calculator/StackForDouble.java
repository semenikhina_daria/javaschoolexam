package com.tsystems.javaschool.tasks.calculator;

public class StackForDouble {
    private int maxSize;
    private double[] stackArray;
    private int top;

    public StackForDouble(int max) {
        maxSize = max;
        stackArray = new double[maxSize];
        top = -1;
    }

    public void push(double j) {
        stackArray[++top] = j;
    }

    public double pop() {
        return stackArray[top--];
    }

    public double peek() {
        return stackArray[top];
    }

    public boolean isEmpty() {
        return (top == -1);
    }
}
