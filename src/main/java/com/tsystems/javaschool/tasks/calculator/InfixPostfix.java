package com.tsystems.javaschool.tasks.calculator;

public class InfixPostfix {
    public static int Prec(char ch) {
        switch (ch) {
            case '+':
            case '-':
                return 1;

            case '*':
            case '/':
                return 2;
        }
        return -1;
    }

    public static String infixToPostfix(String exp) {
        String result = new String("");

        StackForChar theStack = new StackForChar(20);

        for (int i = 0; i < exp.length(); ++i) {
            char c = exp.charAt(i);
            String dot = ".";
            String parse = " ";

            if (Character.isAlphabetic(c)) {
                result = "null";
                return result;
            } else
            if (Character.isDigit(c)) {
                result += c;
            } else if (Character.toString(c).equals(dot)) {

                result = result + c;
            } else if (Character.toString(c).equals(parse)) {
                result = result;
            } else if (c == '(')
                theStack.push(c);

            else if (c == ')') {
                while (!theStack.isEmpty() &&
                        theStack.peek() != '(')

                    result = result + " " + theStack.pop();

                theStack.pop();
            } else {
                while (!theStack.isEmpty() && Prec(c)
                        <= Prec(theStack.peek())) {

                    result = result + " " + theStack.pop();
                }
                result += " ";
                theStack.push(c);
            }

        }

        while (!theStack.isEmpty()) {
            if (theStack.peek() == '(')
                return "null";
            result = result + " " + theStack.pop();
        }
        return result;
    }
}