package com.tsystems.javaschool.tasks.calculator;

public class Calculator {
    /*В начале при прочтении задания создалось ошибошное впечатление,
    что нельзя воспользоваться import java.utils.Stack;, поменять это естественно можно, но из-за ограниченного времени
     оставлен вариант с ипользованием собственных стеков,
     В данном репозитории две задачи проходят все тесты - Калькулятор и Подпоследовательность, в связи с ошибками задание
     Пирамида было удалено, для успешного построения проекта. Итог выполнения - 2 задачи из 3.*/
    StackForDouble st = new StackForDouble(50);

    public String evaluate(String input) {
        try {
            String cal = InfixPostfix.infixToPostfix(input);
            if (cal != "null") {

                String[] strings = cal.split(" ");
                Calculator c = new Calculator();
                try {
                    return c.Evaluate(strings);
                } catch (ArrayIndexOutOfBoundsException e) {
                    return null;
                }
            } else {
                return null;
            }

        } catch (ArrayIndexOutOfBoundsException ex) {
            return null;
        } catch (Exception ec) {
            return null;
        }
    }


    public String Evaluate(String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            if (isNumber(strings[i])) {
                st.push(Double.parseDouble(strings[i]));
            } else {
                double tmp1 = st.pop();
                double tmp2 = st.pop();

                switch (strings[i]) {
                    case "+":

                        st.push(tmp1 + tmp2);

                        break;
                    case "-":

                        st.push(tmp2 - tmp1);

                        break;
                    case "*":
                        st.push(tmp1 * tmp2);

                        break;
                    case "/":
                        if (tmp1 == 0) {
                            return null;
                        }
                        st.push(tmp2 / tmp1);


                        break;
                }
            }

        }
        if (!st.isEmpty()) {
            Double res = st.pop();
            String s = String.format("%.4f", res);
            s = NullDeleter(s);
            return s.replaceAll(",", ".");

        }
        return null;
    }

    public static String NullDeleter(String num) {
        int k = 0;
        int decimalIndex = num.indexOf(",");
        for (int i = num.length() - 1; i > decimalIndex; i--) {
            if (num.charAt(i) == '0') {
                k++;
            } else {
                break;
            }
        }
        if (k == 4) {
            k++;
        }
        return num.substring(0, num.length() - k);
    }

    private static boolean isNumber(String strings) {
        try {
            Double.parseDouble(strings);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }


}
